﻿using System;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;

namespace TD8
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Étape 1 : Création du socket
                Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); 
                // Étape 2 : Attachement à un n° de port connu
               IPEndPoint iep = new IPEndPoint(IPAddress.Any, 1212);
               sock.Bind(iep);
               // Étape 3 : Ouverture du service
               sock.Listen(5);
               // Étpe 4 : Prise en charge des clients 
               while (true)
               {
                   // Attente de la connexion d'un client
                   Socket sock_serv = sock.Accept();
                   // Délégation de la gestion du client à un Thread
                   ThreadPool.QueueUserWorkItem(CommunicationClient, sock_serv);
               }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }
        }

        static void CommunicationClient(object state)
        {
            byte[] b_an = new byte[4];
            byte[] b_si = new byte[8];
            Socket S = (Socket) state;
            Console.WriteLine("Client {0}", S.RemoteEndPoint.ToString());
            //Récupération du nombre d'année fourni par le client
            S.Receive(b_an);
            //Récupération du capital initial
            S.Receive(b_si);
            //Extraction des données présentes dans b_an et b_si
            int i_nb_annees = BitConverter.ToInt32(b_an, 0);
            Console.WriteLine("Nombre d'annés : {0}", i_nb_annees);
            double d_si = BitConverter.ToDouble(b_si, 0);
            Console.WriteLine("Capital initial :  {0}", d_si);

            // Calcul du capital final
            for (int i = 0; i < i_nb_annees; i++)
            {
                d_si = d_si + (d_si * 0.75 / 100);
               
            }
            // Mise sous forme tab d'octets du capital final
            byte[] b_sf = BitConverter.GetBytes(d_si);
            // Envoi au client
            S.Send(b_sf);
            //Cloture de la communication
            S.Shutdown(SocketShutdown.Both);
            S.Close();
            }
        }
    }
